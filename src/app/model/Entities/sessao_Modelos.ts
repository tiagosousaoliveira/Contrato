import { clausula_Sessao_Modelos } from "./clausula_Sessao_Modelos"
import { paragrafo_Sessao_Modelos } from "./paragrafo_Sessao_Modelos"


export class sessao_Modelos{

    nome: string
    modeloId : string
    clausula_Sessao_Modelos = Array<clausula_Sessao_Modelos>()
    paragrafo_Sessao_Modelos = Array<paragrafo_Sessao_Modelos>()

    constructor(){
        this.nome= ''
        this.modeloId = ''
    }
}