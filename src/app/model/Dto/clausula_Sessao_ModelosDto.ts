import { paragrafo_Sessao_ModelosDto } from "./paragrafo_Sessao_ModelosDto";

export class clausula_Sessao_ModelosDto {
    
    Nome : string ='';
    Titulo : string ='';
    Caput : string ='';
    Alineas : string ='';
    Rotulo: string ='';
    ordem: number = 0;
    arrayParagrafo = new Array<paragrafo_Sessao_ModelosDto>()

    constructor(){
        
    }
}