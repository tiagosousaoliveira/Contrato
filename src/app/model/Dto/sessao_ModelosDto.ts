import { clausula_Sessao_ModelosDto } from "./clausula_Sessao_ModelosDto"
import { paragrafo_Sessao_ModelosDto } from "./paragrafo_Sessao_ModelosDto"

export class sessao_ModelosDto{

    nome: string
    modeloId : string 
    clausula_Sessao_Modelos = Array<clausula_Sessao_ModelosDto>()
    paragrafo_Sessao_Modelos = Array<paragrafo_Sessao_ModelosDto>()

    constructor(){
        this.nome= ''
        this.modeloId = ''
    }
}