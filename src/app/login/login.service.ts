import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { _Shared } from "../Service/shared.service";

@Injectable({
    providedIn: 'root',
  })
export class LoginService {


  constructor(
    private http: _Shared){
  }

  post(body:any): Observable<any>  {
    return this.http.Post(body,`login`);
  }  
}