import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;

  constructor(private service: LoginService,
    private formbuild: FormBuilder,
    private routerActiv: ActivatedRoute,
    private router: Router) {


    this.formLogin = this.formbuild.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
    
  }


  ngOnInit(): void {

  }


  submit() {
    
    this.service.post(this.mapper(this.formLogin.controls))
    .subscribe(res => {
      this.router.navigate(['/home'])
    }, error => {
      
    })
    this.router.navigate(['/home'])
  }

   mapper(form: any): any{

    return null
    
   }

}