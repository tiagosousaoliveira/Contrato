import { NgModule } from "@angular/core";
import { PeopleComponent } from './people/people.component';
import { VariableComponent } from './variable/variable.component';
import { ClassificationTypeComponent } from './classification-type/classification-type.component';
import { DocumentComponent } from './document/document.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { ClauseComponent } from './clause/clause.component';
import { ComponentsComponent } from "./components.component";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatListModule} from '@angular/material/list';
import {MatSliderModule } from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';


import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';
import { CommonModule } from "@angular/common";
import {MatSelectModule} from '@angular/material/select';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ModalClauseComponent } from "./modal/modal-clause/modal-clause.component";
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTabsModule} from '@angular/material/tabs';
import { TampletComponent } from "./tamplet/tamplet.component";
import { ModalParagrafoComponent } from "./modal/modal-paragrafo/modal-paragrafo.component";
import { DocumentModelComponent } from "./document-model/document-model.component";
import { ServicoClausula } from "./clause/clause.service";
import { ServicoClassificacao } from "./classification-type/classification.service";
import { ServicoDocument } from "./document/document.service";


@NgModule({


  declarations: [
    TampletComponent,
    PeopleComponent,
    VariableComponent,
    ClassificationTypeComponent,
    DocumentModelComponent,
    DocumentComponent,
    PaymentMethodComponent,
    ClauseComponent,
    ComponentsComponent,
    ModalClauseComponent,
    ModalParagrafoComponent
  ],
  exports:[],
  
  imports: [
    MatDialogModule,
    MatGridListModule,
    MatSelectModule,
    MatExpansionModule,
    MatTooltipModule,
    MatTabsModule,
   // ComponentRouterModule,
    CommonModule,
    FormsModule,
    MatSliderModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatListModule,
    MatInputModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatTableModule,
  ],
  entryComponents:[

  ],
  providers:[
    ServicoClausula,
    ServicoClassificacao,
    ServicoDocument,
  ]
})


export class ComponentModule{ }