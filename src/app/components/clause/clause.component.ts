import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalClauseComponent } from '../modal/modal-clause/modal-clause.component';
import { ModalParagrafoComponent } from '../modal/modal-paragrafo/modal-paragrafo.component';

@Component({
  selector: 'app-clause',
  templateUrl: './clause.component.html',
  styleUrls: ['./clause.component.scss']
})
export class ClauseComponent implements OnInit {
  panelOpenState = false;

  paragrafo: string = '';
  ordem: string = '';
  indice =0;

  arrayParagrafo: any[];

  constructor(public dialog: MatDialog) { 
    this.arrayParagrafo = new Array<any>();
  }

  ngOnInit(): void {
  }

openDialog(_paragrafo = null, indice = -1): void {

    var paragrafoedicao = _paragrafo
    this.paragrafo = ''
    const dialogRef = this.dialog.open(ModalParagrafoComponent, {
      width: '55%',
      height: '55%',
      data: (paragrafoedicao == null ? 
        {ordem: this.ordem, paragrafo: this.paragrafo, indice: indice} : 
        {ordem: this.ordem, paragrafo: paragrafoedicao, indice: indice})
    });

    dialogRef.afterClosed().subscribe(result => {

      if(result){
        this.paragrafo = result.paragrafo;
        if(result.indice == -1) {
          this.arrayParagrafo.push(this.paragrafo)
        }else{
          var indice = result.indice
          this.arrayParagrafo.splice(indice,1,this.paragrafo)
        }
        console.log('The dialog was closed' + this.paragrafo );
        console.log(this.arrayParagrafo)
      }
    });

  }



  editeParagrafo(indice: any){
    var paragrafo_ = this.arrayParagrafo.find((element, indi) => indi == indice)
    this.openDialog(paragrafo_,indice);
  }

  removerParagrafo(indice: any){
    this.arrayParagrafo.splice(indice, 1)
  }

  CopiarParagrafo(indice: any){
    var paragrafo_ = this.arrayParagrafo.find((element, indi) => indi == indice)
    this.arrayParagrafo.push(paragrafo_)
  }
  
}
