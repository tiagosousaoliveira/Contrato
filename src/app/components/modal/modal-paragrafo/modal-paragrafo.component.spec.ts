import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalParagrafoComponent } from './modal-paragrafo.component';

describe('ModalParagrafoComponent', () => {
  let component: ModalParagrafoComponent;
  let fixture: ComponentFixture<ModalParagrafoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalParagrafoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalParagrafoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
