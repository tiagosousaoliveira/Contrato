

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  paragrafo: string;
  ordem: string;
  tipo: number;
}


@Component({
  selector: 'app-modal-paragrafo',
  templateUrl: './modal-paragrafo.component.html',
  styleUrls: ['./modal-paragrafo.component.scss']
})
export class ModalParagrafoComponent  implements OnInit {

  paragrafo = ''
  constructor(
    public dialogRef: MatDialogRef<ModalParagrafoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}



  ngOnInit(): void {
    this.paragrafo = this.data['paragrafo']
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
