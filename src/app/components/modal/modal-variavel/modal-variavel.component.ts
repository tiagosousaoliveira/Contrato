import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface DialogData {
  variavel: any;
  mascara: any;
  tipo: number;
}
@Component({
  selector: 'app-modal-variavel',
  templateUrl: './modal-variavel.component.html',
  styleUrls: ['./modal-variavel.component.scss']
})
export class ModalVariavelComponent implements OnInit {


  arrayVariavel: any[]

  constructor(
    public dialogRef: MatDialogRef<ModalVariavelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.arrayVariavel = new Array<any>()
    }

  ngOnInit(): void {
    this.arrayVariavel = this.data['variavel']
  }

  incluirVariavel(variavel: any){

  }
}



