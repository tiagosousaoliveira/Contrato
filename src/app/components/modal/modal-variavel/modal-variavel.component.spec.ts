import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalVariavelComponent } from './modal-variavel.component';

describe('ModalVariavelComponent', () => {
  let component: ModalVariavelComponent;
  let fixture: ComponentFixture<ModalVariavelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalVariavelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalVariavelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
