import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServicoClausula } from '../../clause/clause.service';

export interface DialogData {
  clausula: any;
  name: string;
  tipo: number;
}
@Component({
  selector: 'app-modal-clause',
  templateUrl: './modal-clause.component.html',
  styleUrls: ['./modal-clause.component.scss']
})
export class ModalClauseComponent implements OnInit {

  toppings = new FormControl();
  
  arrayClausula: any[];
  arraysubclausula: any[]

  constructor(private servico : ServicoClausula,
    public dialogRef: MatDialogRef<ModalClauseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.arrayClausula = new Array<any>()
      this.arraysubclausula = new Array<any>()
    }



  ngOnInit(): void {
    this.arrayClausula = this.data['clausula']
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarClausula(){
    this.servico.Get(null).subscribe(res =>{
      console.log(res)
    }, error =>{
      console.log(error)
    })
  }

  incluirclausula(arrayClausula : any[]){

    this.arraysubclausula = new Array<any>()
    arrayClausula.forEach(element => {
      this.arraysubclausula.push(element)
    }); 

    this.data['clausula'] = this.arraysubclausula
  }

  removerClausula(indice : any){
    this.arraysubclausula.splice(indice, 1)
  }
  

}

