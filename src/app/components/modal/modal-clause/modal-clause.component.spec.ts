import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalClauseComponent } from './modal-clause.component';

describe('ModalClauseComponent', () => {
  let component: ModalClauseComponent;
  let fixture: ComponentFixture<ModalClauseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalClauseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalClauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
