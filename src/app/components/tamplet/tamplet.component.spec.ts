import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TampletComponent } from './tamplet.component';

describe('TampletComponent', () => {
  let component: TampletComponent;
  let fixture: ComponentFixture<TampletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TampletComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TampletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
