import { Component, Input, OnInit, Output } from '@angular/core';
import { ClausulaDto } from 'src/app/model/Dto/ClausulaDto';

@Component({
  selector: 'app-tamplet',
  templateUrl: './tamplet.component.html',
  styleUrls: ['./tamplet.component.scss']
})
export class TampletComponent implements OnInit {
  
  panelOpenState =false
  arrayParagrafo: any[] = new Array<any>()

  @Input() paragrafo = ''
  @Input() tipo      = 0

  @Input() clausula : ClausulaDto = new ClausulaDto()
  
  
  constructor() { }

  ngOnInit(): void {
    console.log(this.paragrafo,this.tipo)
  }


  openDialog(){

  }


  editeParagrafo(indice : any){

  }
  removerParagrafo(indice : any){

  }
  CopiarParagrafo(indice : any){

  }
}
