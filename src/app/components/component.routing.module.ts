import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClassificationTypeComponent } from "./classification-type/classification-type.component";
import { ClauseComponent } from "./clause/clause.component";
import { ComponentsComponent } from "./components.component";
import { DocumentComponent } from "./document/document.component";
//import { ModelComponent } from "./model/model.component";
import { PaymentMethodComponent } from "./payment-method/payment-method.component";
import { PeopleComponent } from "./people/people.component";
import { VariableComponent } from "./variable/variable.component";

const listroutes: Routes = [
  {
    path: 'lista', component: ComponentsComponent,
    children: [
      { path: 'pessoa', component: PeopleComponent },
      { path: 'clausula', component: ClauseComponent },
      { path: 'classificacao', component: ClassificationTypeComponent },
    //  { path: 'modelo', component: ModelComponent },
      { path: 'pagamento', component: PaymentMethodComponent },
      { path: 'variavel', component: VariableComponent },
      { path: 'documento', component: DocumentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(listroutes)],
  exports: [RouterModule]
})
export class ComponentRouterModule { }