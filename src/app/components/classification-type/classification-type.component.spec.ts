import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationTypeComponent } from './classification-type.component';

describe('ClassificationTypeComponent', () => {
  let component: ClassificationTypeComponent;
  let fixture: ComponentFixture<ClassificationTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassificationTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
