import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ClausulaDto } from 'src/app/model/Dto/ClausulaDto';
import { clausula_Sessao_ModelosDto } from 'src/app/model/Dto/clausula_Sessao_ModelosDto';
import { Item } from 'src/app/model/Dto/Item';
import { paragrafo_Sessao_ModelosDto } from 'src/app/model/Dto/paragrafo_Sessao_ModelosDto';
import { sessao_ModelosDto } from 'src/app/model/Dto/sessao_ModelosDto';
import { sessao_Modelos } from 'src/app/model/Entities/sessao_Modelos';
import { ModalClauseComponent } from '../modal/modal-clause/modal-clause.component';
import { ModalParagrafoComponent } from '../modal/modal-paragrafo/modal-paragrafo.component';

@Component({
  selector: 'app-document-model',
  templateUrl: './document-model.component.html',
  styleUrls: ['./document-model.component.scss']
})
export class DocumentModelComponent implements OnInit {

  @Output() valor = ''
  @Output() tipo = 0

  formModel: FormGroup;

  status: boolean = false;
  panelOpenState = false;


  paragrafo = ''
  clausula: ClausulaDto;
  ordem = 0


  arraySessao: sessao_ModelosDto[]
  arrayVariavel: String[]
  arrayClausula: ClausulaDto[]
  arrayClausulaModal: ClausulaDto[]


  sessao: sessao_ModelosDto
  clausulasessao: clausula_Sessao_ModelosDto
  paragrafosessao: paragrafo_Sessao_ModelosDto


  constructor(
    private formBuild: FormBuilder,
    public dialog: MatDialog) {

    this.arraySessao = new Array<sessao_ModelosDto>();
    this.arrayVariavel = new Array<String>();
    this.arrayClausula = new Array<ClausulaDto>();
    this.arrayClausulaModal = new Array<ClausulaDto>();

    this.sessao = new sessao_ModelosDto()
    this.clausula = new ClausulaDto()
    this.clausulasessao = new clausula_Sessao_ModelosDto()
    this.paragrafosessao = new paragrafo_Sessao_ModelosDto()

    this.formModel = this.formBuild.group({
      nome: [''],
      tipo: [''],
      variavel: [''],
      sessao: ['']
    })
  }

  ngOnInit() {
    this.initClausula()
  }

  gerarModelo() {
    if (this.status == true) {
    } else {
      this.status = true
    }
  }

  CancelarModelo() {
    this.status = false
  }

  incluirSessao() {
    this.sessao = new sessao_ModelosDto()
    this.sessao.nome = this.formModel.value.sessao

    this.arraySessao.push(this.sessao)
    console.log(this.arraySessao)
    this.formModel.patchValue({
      sessao: ''
    })
  }

  incluirVariavel() {
    var variavel = this.formModel.value.variavel
    this.arrayVariavel.push(variavel)
    this.formModel.patchValue({
      variavel: ''
    })
  }

  removerSessao(sessao: any) {
    this.arraySessao.splice(sessao, 1)
  }

  removerVariavel(indice: any) {
    this.arrayVariavel.splice(indice, 1)
  }




  inserirParagrafo(indice: any) {
    this.openDialogParagrafo(null, -1, indice)
  }
  inserirClausula(indice: any) {
    this.openDialogClausula(null, -1, indice)
  }


  openDialogParagrafo(_paragrafo = null, indice = -1, sessaoNome: any): void {

    var paragrafoedicao = _paragrafo
    const dialogRef = this.dialog.open(ModalParagrafoComponent, {
      width: '55%',
      height: '55%',
      data: (paragrafoedicao == null ?
        { paragrafo: '', indice: indice } :
        { paragrafo: paragrafoedicao, indice: indice })
    });

    dialogRef.afterClosed().subscribe(result => {
      debugger
      if (result) {
        this.paragrafosessao = new paragrafo_Sessao_ModelosDto()
        this.paragrafosessao.descricao = result.paragrafo;
        this.paragrafosessao.ordem = result.ordem

        if (result.indice == -1) {


          this.arraySessao.forEach(element => {
            if (element.nome == sessaoNome) {
              element.paragrafo_Sessao_Modelos.push(this.paragrafosessao)
            }

          })

          console.log(this.arraySessao)

        } else {
          var indice = result.indice
          this.arraySessao.forEach(element => {
            if (element.nome == sessaoNome) {
              element.paragrafo_Sessao_Modelos.splice(indice, 1, this.paragrafosessao)
            }
          })

        }
      }

    });

  }

  openDialogClausula(_clausula = null, indice = -1, sessaoNome: any): void {
    var clausula_ = _clausula

    const dialogRef = this.dialog.open(ModalClauseComponent, {
      width: '35%',
      height: '35%',
      data: (clausula_ == null ?
        { clausula: this.arrayClausula, indice: indice } :
        { clausula: clausula_, indice: indice })
    });

    dialogRef.afterClosed().subscribe(result => {
      debugger
      if (result) {
        
        this.arrayClausulaModal = new Array<ClausulaDto>()
        this.arrayClausulaModal = result.clausula

        if (result.indice == -1) {

          this.arraySessao.forEach(elementsessao => {
            if (elementsessao.nome == sessaoNome) {
              this.arrayClausulaModal.forEach(element =>{
                elementsessao.clausula_Sessao_Modelos.push(element)
              })
              
            }

          })

        } else {
          var indice = result.indice
        
        }
      }

    });
  }



  initClausula(){
    this.clausula = new ClausulaDto()

    this.clausula.Alineas= 'Insercao de Alinear';
    this.clausula.Caput = 'Insercao de Caput';
    this.clausula.Nome = 'Insercao de Nome';
    this.clausula.Rotulo = 'Insercao de Rotulo';
    this.clausula.Titulo= 'Insercao de Titulo';

    this.paragrafosessao= new paragrafo_Sessao_ModelosDto()
    this.paragrafosessao.descricao = 'Teste de inclusão de paragrafo',
    this.paragrafosessao.ordem = 1

    this.clausula.arrayParagrafo.push( this.paragrafosessao)

    var clausula2 = new ClausulaDto()

    clausula2.Alineas= 'Insercao de Alinear';
    clausula2.Caput = 'Insercao de Caput';
    clausula2.Nome = 'Insercao de Nome';
    clausula2.Rotulo = 'Insercao de Rotulo';
    clausula2.Titulo= 'Insercao de Titulo';

    this.paragrafosessao= new paragrafo_Sessao_ModelosDto()
    this.paragrafosessao.descricao = 'Teste de inclusão de paragrafo',
    this.paragrafosessao.ordem = 2



    clausula2.arrayParagrafo.push( this.paragrafosessao)

  
    this.arrayClausula.push(this.clausula)
    this.arrayClausula.push(clausula2)

    console.log(this.arrayClausula)

  }
}
