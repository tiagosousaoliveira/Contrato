import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

export interface PeriodicElement {
  nome: string;
  id: number;
  acao: any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, nome: 'Tiago Sousa de Oliveira', acao: '<i class="fas fa-pen"></i>'},
  {id: 2, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 3, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 4, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 5, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 6, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 7, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 8, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 10, nome: 'Tiago Sousa de Oliveira',acao: 'acao'},
  {id: 4, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 5, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 6, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 7, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 8, nome: 'Tiago Sousa de Oliveira', acao: 'acao'},
  {id: 10, nome: 'Tiago Sousa de Oliveira',acao: 'acao'},
  
  

];


@Component({
  selector: 'app-components',
  templateUrl:'./components.component.html',
  styleUrls: ['./components.component.scss'],
  
})
export class ComponentsComponent implements OnInit {

  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  displayedColumns: string[] = ['id', 'nome','acao'];
  dataSource = ELEMENT_DATA;

  path: any
  resultsLength = 0;

  objeto = {
    id:0,
    nome:""
  }
  
  clickedRows = new Set<PeriodicElement>();

  constructor(private route_path: ActivatedRoute,
    private route: Router) { }

  ngOnInit(): void {
    debugger
    this.getParans()
  }

  getParans(){
    this.path = this.route_path.snapshot.params.path
    console.log(this.path )
  }



  editarposicao(objeto:any){
    this.objeto = objeto
    this.route.navigate(['home/lista/'+this.objeto.id+'/'+this.path] );
  }

  excluirposicao(objeto = null){
    console.log('exclui'  + objeto)
  }

  copiarposicao(objeto = null){
    console.log('copiei' + objeto)
  }

  CadastrarNovo(){
    this.route.navigate(['home/lista/'+'novo'+'/'+this.path] );
  }

  ngAfterContentChecked(){
    debugger
    this.getParans()
  }


  exit(){
    this.route.navigate(['home'] );
  }
}
