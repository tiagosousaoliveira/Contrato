import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


@Injectable()

export class _Shared{
  
  urlBase: string
  
  constructor(private http: HttpClient) 
  {
    this.urlBase = (`${environment.ApiUrl}`);  
  }


  private createHeader(): HttpHeaders{
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
    return new HttpHeaders(headers);
  }



  Get(url:any): Observable<any>{

    return this.http.get(
      this.urlBase+`${url}`
    );
  }

  Post(url:any, body:any): Observable<any>{

    return this.http.post(
      this.urlBase+`${url}`,
      JSON.stringify(body),  
      {headers:this.createHeader()}
    );

  }

  Put(url:any, body:any): Observable<any>{

    return this.http.put(
      this.urlBase+`${url}`,
      JSON.stringify(body),
      {headers:this.createHeader()}
    );
  }

  Delete(url:any , body: any): Observable<any>{
    return this.http.post(
      this.urlBase+`${url}`,
      JSON.stringify(body),  
      {headers:this.createHeader()}
    );
  }
}