import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassificationTypeComponent } from './components/classification-type/classification-type.component';
import { ClauseComponent } from './components/clause/clause.component';
import { ComponentsComponent } from './components/components.component';
import { DocumentModelComponent } from './components/document-model/document-model.component';
import { DocumentComponent } from './components/document/document.component';
import { PaymentMethodComponent } from './components/payment-method/payment-method.component';
import { PeopleComponent } from './components/people/people.component';
import { VariableComponent } from './components/variable/variable.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [


  {
    path: 'home', component: HomeComponent, 
    children: 
    [
      { path: 'login', component: LoginComponent },
      { path: 'lista/:path', component: ComponentsComponent},
      { path: 'lista/:id/pessoa', component: PeopleComponent},
      { path: 'lista/:id/variavel', component: VariableComponent},
      { path: 'lista/:id/classificacao', component: ClassificationTypeComponent},
      { path: 'lista/:id/clausula', component: ClauseComponent},
      { path: 'lista/:id/documento', component: DocumentComponent},
      { path: 'lista/:id/modelo', component: DocumentModelComponent},
      { path: 'lista/:id/pagamento', component: PaymentMethodComponent},

    ]
  },



  { path: '', redirectTo: 'home/login', pathMatch: 'full' },
  { path: '*', redirectTo: 'login', }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


// children: [
//   { path: 'pessoa', component: PeopleComponent },
//   { path: 'clausula', component: ClauseComponent },
//   { path: 'classificacao', component: ClassificationTypeComponent },
//   { path: 'modelo', component: ModelComponent },
//   { path: 'pagamento', component: PaymentMethodComponent },
//   { path: 'variavel', component: VariableComponent },
//   { path: 'documento', component: DocumentComponent }